```
 ____        _____       _____
/ ___|      |_   _|     | ____|
\___ \        | |       |  _|
 ___) |  _    | |    _  | |___
|____/  (_)   |_|   (_) |_____|
Simple      Text        Editor
       By: Breadleaf
```

### todo:

- [X] add better file selection using cli args
- [ ] remove python errors and convert to user friendly ones
- [X] fix Makefile to install the file to ~/.local/bin/
- [ ] add auto update and uninstall to makefile
- [ ] incorperate better file handeling
