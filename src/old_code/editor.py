#!/usr/bin/python3

import subprocess
import curses
import os

def editor(path):
    lines = []

    with open(path) as file:
        for line in file:
            lines.append(line)

    current_line = int(input("enter line to edit: "))

    line_number = 1
    reset = "\033[0;0m"
    for line in lines:
        text_color = ""
        if current_line == line_number:
            text_color = "\033[0;36;40m"
        else:
            text_color = "\033[0;30;46m"
        print(f"{text_color}{line_number}{reset} {text_color}{line.rstrip()}{reset}")
        line_number += 1


def main():
    editor("./example-code/main.cpp")


if __name__ == "__main__":
    main()
