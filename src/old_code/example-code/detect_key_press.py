import termios
import sys
import os

def main():
    old = termios.tcgetattr(sys.stdin)
    x = 0
    while x != chr(27):
        x = sys.stdin.read(1)[0]
        print(f"You pressed {x}")

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old)


if __name__ == "__main__":
    main()
