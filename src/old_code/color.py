#/usr/bin/python3

class color:
    # Normal Colors
    red = "\033[0;31m"
    yellow = "\033[0;33m"
    green = "\033[0;32m"
    blue = "\033[0;34m"
    purple = "\033[0;35m"
    cyan = "\033[0;36m"
    white = "\033[0;37m"
    black = "\033[0;30m"

    # Bold Colors
    bold_red = "\033[1;31m"
    bold_yellow = "\033[1;33m"
    bold_green = "\033[1;32m"
    bold_blue = "\033[1;34m"
    bold_purple = "\033[1;35m"
    bold_cyan = "\033[1;36m"
    bold_white = "\033[1;37m"
    bold_black = "\033[1;30m"

    # Dim Colors
    dim_red = "\033[2;31m"
    dim_yellow = "\033[2;33m"
    dim_green = "\033[2;32m"
    dim_blue = "\033[2;34m"
    dim_purple = "\033[2;35m"
    dim_cyan = "\033[2;36m"
    dim_white = "\033[2;37m"
    dim_black = "\033[2;30m"

    # Italic Colors
    italic_red = "\033[3;31m"
    italic_yellow = "\033[3;33m"
    italic_green = "\033[3;32m"
    italic_blue = "\033[3;34m"
    italic_purple = "\033[3;35m"
    italic_cyan = "\033[3;36m"
    italic_white = "\033[3;37m"
    italic_black = "\033[3;30m"

    # Underlined Colors
    underlined_red = "\033[4;31m"
    underlined_yellow = "\033[4;33m"
    underlined_green = "\033[4;32m"
    underlined_blue = "\033[4;34m"
    underlined_purple = "\033[4;35m"
    underlined_cyan = "\033[4;36m"
    underlined_white = "\033[4;37m"
    underlined_black = "\033[4;30m"

    reset = "\033[m"


colorEngine = lambda string, colour : colour + string + color.reset
