#!/usr/bin/bash

from color import colorEngine # Used to print an error code
from color import color       # Used to obtain colors for colorEngine
from os.path import exists    # Used to check if a directory exists
from os.path import dirname   # Used to extract the path
from os import makedirs       # Used to create a path of directories

def fileMaker(path):
    try:
        f = open(path, "a")
        f.close()
    except IsADirectoryError:
        print(colorEngine("Error: no file in path", color.red))
    except FileNotFoundError:
        # Get the path to the file
        # ie: ./dir/file.txt -> ./dir
        directory = dirname(path)

        # Check if the path exists; if not make it
        if not exists(directory):
            makedirs(directory)

        # Create the file if it doesn't already exist
        f = open(path, "w")
        f.close()


def main():
    f''



if __name__ == "__main__":
    main()
