#/usr/bin/python3

from editor import editor       # Used to edit files
from fileMaker import fileMaker # Used to check for file if doesn't exist make
from getopt import GetoptError  # Used to check for parsing error (see below)
from getopt import getopt       # Used to parse sys.argv (see import sys)
from color import colorEngine   # Used for help message
from color import color         # Used to provide color for colorEngine
import sys                      # Used to optain flags and arguments

def main(argv):
    debugMode = False
    ste_help = (f"usage:\n  -h -> help\n  -i -> input\n"
                f"ste -i {colorEngine('<input>', color.green)} "
                f"{colorEngine('OR', color.underlined_red)} "
                f"ste {colorEngine('<input>', color.green)}")

    try:
        opts, args = getopt(argv, "hi:")
    except GetoptError:
        print(ste_help)
        exit(0)

    if debugMode:
        print("opts:", opts)
        print("args:", args)

    for opt, arg in opts:
        if debugMode:
            print("opt:", opt)
            print("arg:", arg)

        if opt == "-h":
            print(ste_help)
        elif opt == "-i":
            try:
                fileMaker(arg)
                editor(arg)
            except PermissionError:
                print(colorEngine("Error: root permission required", color.red))

if __name__ == "__main__":
    main(sys.argv[1:])
