from os import get_terminal_size
from os.path import exists
from sys import argv

def display(buffer, total_lines, buffer_offset):
    buffer_length = len(buffer)
    line_number = 1
    remaining_lines = total_lines - buffer_length - 1
    pad = int(len(str(buffer_length)))

    if total_lines > buffer_length:
        for line in buffer:
            print(f"{line_number:{pad}} {line}")
            line_number += 1
        for _ in range(remaining_lines):
            print("~")

    if buffer_length >= total_lines:
        line_number += buffer_offset
        for i in range(0 + buffer_offset, total_lines + buffer_offset - 1):
            print(f"{line_number:{pad}} {buffer[i]}")
            line_number += 1

def convert_file_to_buffer(file_path):
    buffer_to_return = []
    if not exists(file_path):
        open(file_path, "w")
    with open(file_path) as f:
        for line in f:
            buffer_to_return.append(line.rstrip())
    if len(buffer_to_return) == 0:
        buffer_to_return.append("")
    return buffer_to_return

def edit_line(buffer, buffer_idx, new_text):
    new_buffer = buffer[:buffer_idx-1]
    new_buffer.append(new_text)
    new_buffer.extend(buffer[buffer_idx:])
    return new_buffer

def delete_line(buffer, buffer_idx):
    return buffer[:buffer_idx-1] + buffer[buffer_idx:]

def insert_new_line(buffer, buffer_idx):
    new_buffer = buffer[:buffer_idx]
    new_buffer.append("")
    new_buffer += buffer[buffer_idx:]
    return new_buffer

def debug_print_buffer(buffer):
    for line in buffer:
        print(line)

def save_buffer_to_file(buffer, file_path):
    with open(file_path, "w") as f:
        for line in buffer:
            f.write(f"{line}\n")

def main(file_name):
    file_buffer = convert_file_to_buffer(file_name)
    original_buffer = file_buffer.copy()
    _, row = get_terminal_size()

    user_input = ""
    offset = 0
    while True:
        error = ""

        if user_input == "up":
            if offset > 0:
                offset -= 1
            else:
                error = "Cannot go up | "

        if user_input == "down":
            if row + offset - 1 != len(file_buffer) and len(file_buffer) > row:
                offset += 1
            else:
                error = "Cannot go down | "

        if user_input.split(" ")[0] == "insert" or user_input.split(" ")[0] == "+":
            file_buffer = insert_new_line(file_buffer, int(user_input.split(" ")[1]))

        if user_input.split(" ")[0] == "delete" or user_input.split(" ")[0] == "-":
            if len(file_buffer) != 1:
                file_buffer = delete_line(file_buffer, int(user_input.split(" ")[1]))
            else:
                error = "Cannot delete | "

        if user_input.split(" ")[0] == "edit" or user_input.split(" ")[0] == "e":
            tmp_data = input(": ")
            file_buffer = edit_line(file_buffer, int(user_input.split(" ")[1]), tmp_data)

        if user_input.split(" ")[0] == "save" or user_input.split(" ")[0] == "s":
                save_buffer_to_file(file_buffer, file_name)
                original_buffer = file_buffer.copy()

        if user_input == "exit":
            if original_buffer != file_buffer:
                while True:
                    tmp = input("Changes have been made. Would you like to save them? [y/n] ")
                    if tmp == "y":
                        save_buffer_to_file(file_buffer, file_name)
                        exit()
                    if tmp == "n":
                        print("Exiting without saving.")
                        exit(0)
            exit(0)

        while True:
            try:
                print(f"[DEBUG] {offset}")
                display(file_buffer, row, offset)
                user_input = input(f"{error}what would you like to do: ")
                break
            except KeyboardInterrupt:
                print()
                continue
            except EOFError:
                print()
                continue
            except Exception as ex:
                print(ex, type(ex))

if __name__ == "__main__":
    if len(argv) != 2:
        print("Error: incorect arg count.\n")
        print("Usage: ste [FILE_NAME]")
        exit(1)
    main(argv[1])
